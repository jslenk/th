package com.tophatter.example.tophatter;

import android.content.*;
import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;

import com.google.common.eventbus.*;

import org.apache.commons.lang.*;
import org.json.*;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ProductDetailsActivity extends AppCompatActivity {

    private JsonModel.Product original;
    private EventBus eventBus;
    public static final String EXTRA_PRODUCT_ORIGINAL_KEY = "original";
    public static final String EXTRA_PRODUCT_UPDATED_KEY = "updated";
    public static final String EXTRA_MESSAGE_KEY = "message";

    private final String getEditTextString(final int id, final String original) {
        String found = original;
        final EditText et = (EditText) findViewById(id);
        if (et != null) {
            found = et.getText().toString();
        }
        return found;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        original = (JsonModel.Product) getIntent().getSerializableExtra(EXTRA_PRODUCT_ORIGINAL_KEY);
        if (original == null) {
            setResult(RESULT_CANCELED);
            finish();
        } else {
            setupOnCreate();
        }
    }

    private void setupOnCreate() {
        eventBus = new EventBus();
        eventBus.register(this);

        ((TextView) findViewById(R.id.details_image_url)).setText(original.getPrimaryImage());
        ((TextView) findViewById(R.id.details_title)).setText(original.getTitle());
        ((TextView) findViewById(R.id.details_description)).setText(original.getDescription());
        ((TextView) findViewById(R.id.details_starting_bid)).setText(String.valueOf(original.getStartingBid()));
        ((TextView) findViewById(R.id.details_buy_now)).setText(String.valueOf(original.getBuyNowPrice()));

        findViewById(R.id.details_cancel_button).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                findViewById(R.id.wait_spinner).setVisibility(View.VISIBLE);
                findViewById(R.id.details).setVisibility(View.GONE);
                setResult(RESULT_CANCELED);
                finish();
                return true;
            }
        });
        findViewById(R.id.details_ok_button).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                findViewById(R.id.wait_spinner).setVisibility(View.VISIBLE);
                findViewById(R.id.details).setVisibility(View.GONE);
                JsonModel.Product updated;
                try {
                    updated = (JsonModel.Product) SerializationUtils.clone(original);
                    updated.setPrimaryImage(getEditTextString(R.id.details_image_url, original.getPrimaryImage()));
                    updated.setTitle(getEditTextString(R.id.details_title, original.getTitle()));
                    updated.setDescription(getEditTextString(R.id.details_description, original.getDescription()));
                    updated.setStartingBid(Integer.parseInt(getEditTextString(R.id.details_starting_bid, String.valueOf(original.getStartingBid()))));
                    updated.setBuyNowPrice(Double.parseDouble(getEditTextString(R.id.details_buy_now, String.valueOf(original.getBuyNowPrice()))));
                    pushJson(updated);
                } catch (Exception exc) {
                    updated = original;
                }
                return true;
            }
        });
    }

    private void pushJson(final JsonModel.Product updated) {
        new UpdateProductController(eventBus, updated).start(MainActivity.api);
    }

    @Subscribe
    public void onResponse(final JsonModel.Product response) {
        final Intent i = new Intent();
        i.putExtra(EXTRA_PRODUCT_UPDATED_KEY, response);
        setResult(RESULT_OK, i);
        finish();
    }

    @Subscribe
    public void onError(final UpdateProductController.UpdateError err) {
        final Intent i = new Intent();
        try {
            JSONObject jObjError = new JSONObject(err.getResponse().errorBody().string());
            i.putExtra(EXTRA_MESSAGE_KEY, jObjError.getString("message"));
        } catch (Exception exc) {
            // oh well.
        }
        setResult(RESULT_CANCELED, i);
        finish();
    }
}
