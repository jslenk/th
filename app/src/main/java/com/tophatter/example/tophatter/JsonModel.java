package com.tophatter.example.tophatter;

import com.google.gson.annotations.*;

import java.io.*;
import java.util.*;

public class JsonModel {

    public class ImageUrl implements Serializable {

        String thumbnail;
        String square;
        String medium;
        String large;
        String original;

        public ImageUrl(String thumbnail, String square, String medium, String large, String original) {
            this.thumbnail = thumbnail;
            this.square = square;
            this.medium = medium;
            this.large = large;
            this.original = original;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public String getSquare() {
            return square;
        }

        public String getMedium() {
            return medium;
        }

        public String getLarge() {
            return large;
        }

        public String getOriginal() {
            return original;
        }

    }

    public class Product implements Serializable {

        String identifier;
        String category;
        String title;
        String description;
        String condition;
        String brand;
        String material;
        List<ProductVariation> variations;

        @SerializedName("starting_bid")
        Integer startingBid;

        @SerializedName("buy_now_price")
        Double buyNowPrice;

        @SerializedName("retail_price")
        Double retailPrice;

        @SerializedName("cost_basis")
        Double costBasis;

        @SerializedName("shipping_price")
        Double shippingPrice;

        @SerializedName("shipping_origin")
        String shippingOrigin;

        @SerializedName("expedited_shipping_price")
        Double expeditedShippingPrice;

        @SerializedName("days_to_fulfill")
        Integer daysToFulfill;

        @SerializedName("days_to_deliver")
        Integer daysToDeliver;

        @SerializedName("expedited_days_to_deliver")
        Integer expeditedDaysToDeliver;

        Double weight;

        @SerializedName("primary_image")
        String primaryImage;

        @SerializedName("extra_images")
        String extraImages;

        @SerializedName("all_images")
        List<ImageUrl> allImages;

        @SerializedName("created_at")
        String createdAt;

        @SerializedName("updated_at")
        String updatedAt;

        @SerializedName("disabled_at")
        String disabledAt;

        @SerializedName("deleted_at")
        String deletedAt;

        String slug;

        public String getIdentifier() {
            return identifier;
        }

        public String getCategory() {
            return category;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getCondition() {
            return condition;
        }

        public String getBrand() {
            return brand;
        }

        public String getMaterial() {
            return material;
        }

        public List<ProductVariation> getVariations() {
            return variations;
        }

        public Integer getStartingBid() {
            return startingBid;
        }

        public Double getBuyNowPrice() {
            return buyNowPrice;
        }

        public Double getRetailPrice() {
            return retailPrice;
        }

        public Double getCostBasis() {
            return costBasis;
        }

        public Double getShippingPrice() {
            return shippingPrice;
        }

        public String getShippingOrigin() {
            return shippingOrigin;
        }

        public Double getExpeditedShippingPrice() {
            return expeditedShippingPrice;
        }

        public Integer getDaysToFulfill() {
            return daysToFulfill;
        }

        public Integer getDaysToDeliver() {
            return daysToDeliver;
        }

        public Integer getExpeditedDaysToDeliver() {
            return expeditedDaysToDeliver;
        }

        public Double getWeight() {
            return weight;
        }

        public String getPrimaryImage() {
            return primaryImage;
        }

        public String getExtraImages() {
            return extraImages;
        }

        public List<ImageUrl> getAllImages() {
            return allImages;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public String getDisabledAt() {
            return disabledAt;
        }

        public String getDeletedAt() {
            return deletedAt;
        }

        public String getSlug() {
            return slug;
        }

        public void setIdentifier(String identifier) {
            this.identifier = identifier;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setCondition(String condition) {
            this.condition = condition;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public void setMaterial(String material) {
            this.material = material;
        }

        public void setVariations(List<ProductVariation> variations) {
            this.variations = variations;
        }

        public void setStartingBid(Integer startingBid) {
            this.startingBid = startingBid;
        }

        public void setBuyNowPrice(Double buyNowPrice) {
            this.buyNowPrice = buyNowPrice;
        }

        public void setRetailPrice(Double retailPrice) {
            this.retailPrice = retailPrice;
        }

        public void setCostBasis(Double costBasis) {
            this.costBasis = costBasis;
        }

        public void setShippingPrice(Double shippingPrice) {
            this.shippingPrice = shippingPrice;
        }

        public void setShippingOrigin(String shippingOrigin) {
            this.shippingOrigin = shippingOrigin;
        }

        public void setExpeditedShippingPrice(Double expeditedShippingPrice) {
            this.expeditedShippingPrice = expeditedShippingPrice;
        }

        public void setDaysToFulfill(Integer daysToFulfill) {
            this.daysToFulfill = daysToFulfill;
        }

        public void setDaysToDeliver(Integer daysToDeliver) {
            this.daysToDeliver = daysToDeliver;
        }

        public void setExpeditedDaysToDeliver(Integer expeditedDaysToDeliver) {
            this.expeditedDaysToDeliver = expeditedDaysToDeliver;
        }

        public void setWeight(Double weight) {
            this.weight = weight;
        }

        public void setPrimaryImage(String primaryImage) {
            this.primaryImage = primaryImage;
        }

        public void setExtraImages(String extraImages) {
            this.extraImages = extraImages;
        }

        public void setAllImages(List<ImageUrl> allImages) {
            this.allImages = allImages;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getId() {
            return getCreatedAt() != null ? getIdentifier() : null;
        }

        public List<String> getImages() {
            return getImages("square");
        }

        public List<String> getImages(String size) {
            List<String> images = new ArrayList<>();
            if (getId() != null) {
                for (ImageUrl image : getAllImages()) {
                    switch (size) {
                        case "thumbnail":
                            images.add(image.getThumbnail());
                            break;
                        case "square":
                            images.add(image.getSquare());
                            break;
                        case "medium":
                            images.add(image.getMedium());
                            break;
                        case "large":
                            images.add(image.getLarge());
                            break;
                        default:
                            images.add(image.getOriginal());
                            break;
                    }
                }
            } else {
                images.add(getPrimaryImage());
                List<String> extraImages = Arrays.asList(getExtraImages().split("|"));
                for (String image : extraImages) {
                    images.add(image);
                }
            }
            return images;
        }

        public Product copy() {
            Product product = new Product();
            product.setCategory(getCategory());
            product.setTitle(getTitle());
            product.setDescription(getDescription());
            product.setCondition(getCondition());
            product.setBrand(getBrand());
            product.setMaterial(getMaterial());
            List<ProductVariation> variations = getVariations();
            List<ProductVariation> variationsCopy = new ArrayList<>();
            for (ProductVariation variation : variations) {
                variationsCopy.add(new ProductVariation(variation.getSize(), variation.getColor(), variation.getQuantity()));
            }
            product.setVariations(variationsCopy);
            product.setStartingBid(getStartingBid());
            product.setBuyNowPrice(getBuyNowPrice());
            product.setRetailPrice(getRetailPrice());
            product.setCostBasis(getCostBasis());
            product.setShippingOrigin(getShippingOrigin());
            product.setShippingPrice(getShippingPrice());
            product.setExpeditedShippingPrice(getExpeditedShippingPrice());
            product.setDaysToFulfill(getDaysToFulfill());
            product.setDaysToDeliver(getDaysToDeliver());
            product.setExpeditedDaysToDeliver(getExpeditedDaysToDeliver());
            product.setWeight(getWeight());
            product.setSlug(getSlug());
            return product;
        }

        public String toParam() {
            return getSlug() != null ? getSlug() : getIdentifier();
        }

    }

    public class ProductVariation implements Serializable {

        String identifier;

        @SerializedName("created_at")
        String createdAt;

        String size;
        String color;
        int quantity;

        public ProductVariation(String size, String color, int quantity) {
            this(null, size, color, quantity);
        }

        public ProductVariation(String identifier, String size, String color, int quantity) {
            this.identifier = identifier;
            this.size = size;
            this.color = color;
            this.quantity = quantity;
        }

        public String getIdentifier() {
            return identifier;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getSize() {
            return size;
        }

        public String getColor() {
            return color;
        }

        public int getQuantity() {
            return quantity;
        }

        public String getId() {
            return getCreatedAt() != null ? getIdentifier() : null;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

    }

/*
    public class AllImage implements Serializable {

        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;
        @SerializedName("square")
        @Expose
        private String square;
        @SerializedName("medium")
        @Expose
        private String medium;
        @SerializedName("large")
        @Expose
        private String large;
        @SerializedName("original")
        @Expose
        private String original;
        private final static long serialVersionUID = -3446494434136365662L;

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public String getSquare() {
            return square;
        }

        public void setSquare(String square) {
            this.square = square;
        }

        public String getMedium() {
            return medium;
        }

        public void setMedium(String medium) {
            this.medium = medium;
        }

        public String getLarge() {
            return large;
        }

        public void setLarge(String large) {
            this.large = large;
        }

        public String getOriginal() {
            return original;
        }

        public void setOriginal(String original) {
            this.original = original;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(thumbnail).append(square).append(medium).append(large).append(original).toHashCode();
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if ((other instanceof AllImage) == false) {
                return false;
            }
            AllImage rhs = ((AllImage) other);
            return new EqualsBuilder().append(thumbnail, rhs.thumbnail).append(square, rhs.square).append(medium, rhs.medium).append(large, rhs.large).append(original, rhs.original).isEquals();
        }

    }

    public class Product implements Serializable {

        @SerializedName("identifier")
        @Expose
        private String identifier;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("condition")
        @Expose
        private String condition;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("material")
        @Expose
        private String material;
        @SerializedName("available_quantity")
        @Expose
        private Long availableQuantity;
        @SerializedName("variations")
        @Expose
        private List<Variation> variations = new ArrayList<Variation>();
        @SerializedName("starting_bid")
        @Expose
        private Double startingBid;
        @SerializedName("buy_now_price")
        @Expose
        private Double buyNowPrice;
        @SerializedName("retail_price")
        @Expose
        private Object retailPrice;
        @SerializedName("cost_basis")
        @Expose
        private Object costBasis;
        @SerializedName("minimum_alerts_needed")
        @Expose
        private Long minimumAlertsNeeded;
        @SerializedName("success_fee_bid")
        @Expose
        private Object successFeeBid;
        @SerializedName("shipping_price")
        @Expose
        private Double shippingPrice;
        @SerializedName("shipping_origin")
        @Expose
        private String shippingOrigin;
        @SerializedName("weight")
        @Expose
        private Object weight;
        @SerializedName("days_to_fulfill")
        @Expose
        private Long daysToFulfill;
        @SerializedName("days_to_deliver")
        @Expose
        private Long daysToDeliver;
        @SerializedName("expedited_shipping_price")
        @Expose
        private Object expeditedShippingPrice;
        @SerializedName("expedited_days_to_deliver")
        @Expose
        private Object expeditedDaysToDeliver;
        @SerializedName("buy_one_get_one_price")
        @Expose
        private Object buyOneGetOnePrice;
        @SerializedName("accessory_price")
        @Expose
        private Object accessoryPrice;
        @SerializedName("accessory_description")
        @Expose
        private Object accessoryDescription;
        @SerializedName("primary_image")
        @Expose
        private String primaryImage;
        @SerializedName("extra_images")
        @Expose
        private String extraImages;
        @SerializedName("all_images")
        @Expose
        private List<AllImage> allImages = new ArrayList<AllImage>();
        @SerializedName("ratings_count")
        @Expose
        private Object ratingsCount;
        @SerializedName("ratings_average")
        @Expose
        private Object ratingsAverage;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("disabled_at")
        @Expose
        private Object disabledAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;
        @SerializedName("blacklisted_at")
        @Expose
        private Object blacklistedAt;
        @SerializedName("admin_hold_at")
        @Expose
        private Object adminHoldAt;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("internal_id")
        @Expose
        private Long internalId;
        private final static long serialVersionUID = -1641860883836829838L;

        public String getIdentifier() {
            return identifier;
        }

        public void setIdentifier(String identifier) {
            this.identifier = identifier;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCondition() {
            return condition;
        }

        public void setCondition(String condition) {
            this.condition = condition;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getMaterial() {
            return material;
        }

        public void setMaterial(String material) {
            this.material = material;
        }

        public Long getAvailableQuantity() {
            return availableQuantity;
        }

        public void setAvailableQuantity(Long availableQuantity) {
            this.availableQuantity = availableQuantity;
        }

        public List<Variation> getVariations() {
            return variations;
        }

        public void setVariations(List<Variation> variations) {
            this.variations = variations;
        }

        public Double getStartingBid() {
            return startingBid;
        }

        public void setStartingBid(Double startingBid) {
            this.startingBid = startingBid;
        }

        public Double getBuyNowPrice() {
            return buyNowPrice;
        }

        public void setBuyNowPrice(Double buyNowPrice) {
            this.buyNowPrice = buyNowPrice;
        }

        public Object getRetailPrice() {
            return retailPrice;
        }

        public void setRetailPrice(Object retailPrice) {
            this.retailPrice = retailPrice;
        }

        public Object getCostBasis() {
            return costBasis;
        }

        public void setCostBasis(Object costBasis) {
            this.costBasis = costBasis;
        }

        public Long getMinimumAlertsNeeded() {
            return minimumAlertsNeeded;
        }

        public void setMinimumAlertsNeeded(Long minimumAlertsNeeded) {
            this.minimumAlertsNeeded = minimumAlertsNeeded;
        }

        public Object getSuccessFeeBid() {
            return successFeeBid;
        }

        public void setSuccessFeeBid(Object successFeeBid) {
            this.successFeeBid = successFeeBid;
        }

        public Double getShippingPrice() {
            return shippingPrice;
        }

        public void setShippingPrice(Double shippingPrice) {
            this.shippingPrice = shippingPrice;
        }

        public String getShippingOrigin() {
            return shippingOrigin;
        }

        public void setShippingOrigin(String shippingOrigin) {
            this.shippingOrigin = shippingOrigin;
        }

        public Object getWeight() {
            return weight;
        }

        public void setWeight(Object weight) {
            this.weight = weight;
        }

        public Long getDaysToFulfill() {
            return daysToFulfill;
        }

        public void setDaysToFulfill(Long daysToFulfill) {
            this.daysToFulfill = daysToFulfill;
        }

        public Long getDaysToDeliver() {
            return daysToDeliver;
        }

        public void setDaysToDeliver(Long daysToDeliver) {
            this.daysToDeliver = daysToDeliver;
        }

        public Object getExpeditedShippingPrice() {
            return expeditedShippingPrice;
        }

        public void setExpeditedShippingPrice(Object expeditedShippingPrice) {
            this.expeditedShippingPrice = expeditedShippingPrice;
        }

        public Object getExpeditedDaysToDeliver() {
            return expeditedDaysToDeliver;
        }

        public void setExpeditedDaysToDeliver(Object expeditedDaysToDeliver) {
            this.expeditedDaysToDeliver = expeditedDaysToDeliver;
        }

        public Object getBuyOneGetOnePrice() {
            return buyOneGetOnePrice;
        }

        public void setBuyOneGetOnePrice(Object buyOneGetOnePrice) {
            this.buyOneGetOnePrice = buyOneGetOnePrice;
        }

        public Object getAccessoryPrice() {
            return accessoryPrice;
        }

        public void setAccessoryPrice(Object accessoryPrice) {
            this.accessoryPrice = accessoryPrice;
        }

        public Object getAccessoryDescription() {
            return accessoryDescription;
        }

        public void setAccessoryDescription(Object accessoryDescription) {
            this.accessoryDescription = accessoryDescription;
        }

        public String getPrimaryImage() {
            return primaryImage;
        }

        public void setPrimaryImage(String primaryImage) {
            this.primaryImage = primaryImage;
        }

        public String getExtraImages() {
            return extraImages;
        }

        public void setExtraImages(String extraImages) {
            this.extraImages = extraImages;
        }

        public List<AllImage> getAllImages() {
            return allImages;
        }

        public void setAllImages(List<AllImage> allImages) {
            this.allImages = allImages;
        }

        public Object getRatingsCount() {
            return ratingsCount;
        }

        public void setRatingsCount(Object ratingsCount) {
            this.ratingsCount = ratingsCount;
        }

        public Object getRatingsAverage() {
            return ratingsAverage;
        }

        public void setRatingsAverage(Object ratingsAverage) {
            this.ratingsAverage = ratingsAverage;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDisabledAt() {
            return disabledAt;
        }

        public void setDisabledAt(Object disabledAt) {
            this.disabledAt = disabledAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public Object getBlacklistedAt() {
            return blacklistedAt;
        }

        public void setBlacklistedAt(Object blacklistedAt) {
            this.blacklistedAt = blacklistedAt;
        }

        public Object getAdminHoldAt() {
            return adminHoldAt;
        }

        public void setAdminHoldAt(Object adminHoldAt) {
            this.adminHoldAt = adminHoldAt;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public Long getInternalId() {
            return internalId;
        }

        public void setInternalId(Long internalId) {
            this.internalId = internalId;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(identifier).append(category).append(title).append(description).append(condition).append(brand).append(material).append(availableQuantity).append(variations).append(startingBid).append(buyNowPrice).append(retailPrice).append(costBasis).append(minimumAlertsNeeded).append(successFeeBid).append(shippingPrice).append(shippingOrigin).append(weight).append(daysToFulfill).append(daysToDeliver).append(expeditedShippingPrice).append(expeditedDaysToDeliver).append(buyOneGetOnePrice).append(accessoryPrice).append(accessoryDescription).append(primaryImage).append(extraImages).append(allImages).append(ratingsCount).append(ratingsAverage).append(createdAt).append(updatedAt).append(disabledAt).append(deletedAt).append(blacklistedAt).append(adminHoldAt).append(slug).append(internalId).toHashCode();
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if ((other instanceof Product) == false) {
                return false;
            }
            Product rhs = ((Product) other);
            return new EqualsBuilder().append(identifier, rhs.identifier).append(category, rhs.category).append(title, rhs.title).append(description, rhs.description).append(condition, rhs.condition).append(brand, rhs.brand).append(material, rhs.material).append(availableQuantity, rhs.availableQuantity).append(variations, rhs.variations).append(startingBid, rhs.startingBid).append(buyNowPrice, rhs.buyNowPrice).append(retailPrice, rhs.retailPrice).append(costBasis, rhs.costBasis).append(minimumAlertsNeeded, rhs.minimumAlertsNeeded).append(successFeeBid, rhs.successFeeBid).append(shippingPrice, rhs.shippingPrice).append(shippingOrigin, rhs.shippingOrigin).append(weight, rhs.weight).append(daysToFulfill, rhs.daysToFulfill).append(daysToDeliver, rhs.daysToDeliver).append(expeditedShippingPrice, rhs.expeditedShippingPrice).append(expeditedDaysToDeliver, rhs.expeditedDaysToDeliver).append(buyOneGetOnePrice, rhs.buyOneGetOnePrice).append(accessoryPrice, rhs.accessoryPrice).append(accessoryDescription, rhs.accessoryDescription).append(primaryImage, rhs.primaryImage).append(extraImages, rhs.extraImages).append(allImages, rhs.allImages).append(ratingsCount, rhs.ratingsCount).append(ratingsAverage, rhs.ratingsAverage).append(createdAt, rhs.createdAt).append(updatedAt, rhs.updatedAt).append(disabledAt, rhs.disabledAt).append(deletedAt, rhs.deletedAt).append(blacklistedAt, rhs.blacklistedAt).append(adminHoldAt, rhs.adminHoldAt).append(slug, rhs.slug).append(internalId, rhs.internalId).isEquals();
        }

    }

    public class Variation implements Serializable {

        @SerializedName("identifier")
        @Expose
        private String identifier;
        @SerializedName("size")
        @Expose
        private Object size;
        @SerializedName("color")
        @Expose
        private Object color;
        @SerializedName("quantity")
        @Expose
        private Long quantity;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        private final static long serialVersionUID = 4760754582888722422L;

        public String getIdentifier() {
            return identifier;
        }

        public void setIdentifier(String identifier) {
            this.identifier = identifier;
        }

        public Object getSize() {
            return size;
        }

        public void setSize(Object size) {
            this.size = size;
        }

        public Object getColor() {
            return color;
        }

        public void setColor(Object color) {
            this.color = color;
        }

        public Long getQuantity() {
            return quantity;
        }

        public void setQuantity(Long quantity) {
            this.quantity = quantity;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(identifier).append(size).append(color).append(quantity).append(createdAt).toHashCode();
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if ((other instanceof Variation) == false) {
                return false;
            }
            Variation rhs = ((Variation) other);
            return new EqualsBuilder().append(identifier, rhs.identifier).append(size, rhs.size).append(color, rhs.color).append(quantity, rhs.quantity).append(createdAt, rhs.createdAt).isEquals();
        }

    }
*/
}