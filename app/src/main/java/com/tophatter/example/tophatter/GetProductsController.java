package com.tophatter.example.tophatter;

import android.util.*;

import com.google.common.eventbus.*;

import java.util.*;

import retrofit2.*;

public class GetProductsController implements Callback<List<JsonModel.Product>> {

    public static EventBus eventBus = MainActivity.eventBus; // todo: via DI framework.

    public void start(final EndpointApi api) {
        Call<List<JsonModel.Product>> call = api.getProducts();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<JsonModel.Product>> call, Response<List<JsonModel.Product>> response) {
        if (response.isSuccessful()) {
            List<JsonModel.Product> l = response.body();
//            // todo: remove this testing hack.
//            l = Lists.newArrayList(Iterables.filter(l, new Predicate<JsonModel.Product>() {
//                @Override
//                public boolean apply(JsonModel.Product input) {
//                    return input.getIdentifier().equals("RING-1");
//                }
//            }));
            eventBus.post(l);
        } else {
            Log.e("TAG", response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<List<JsonModel.Product>> call, Throwable t) {
        Log.e("TAG", Arrays.toString(t.getStackTrace()));
    }
}
