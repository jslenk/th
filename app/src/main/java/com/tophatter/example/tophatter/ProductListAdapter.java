package com.tophatter.example.tophatter;

import android.content.*;
import android.graphics.*;
import android.view.*;
import android.widget.*;

import com.squareup.picasso.*;

import java.util.*;

class ProductListAdapter extends ArrayAdapter<JsonModel.Product> {

    private static final int BgColor1 = Color.argb(8, 255, 128, 128);
    private static final int BgColor2 = Color.argb(8, 128, 128, 255);

    public ProductListAdapter(Context context, List products) {
        super(context, 0, products);
    }

    @Override
    public View getView(int i, View v, ViewGroup g) {
        JsonModel.Product p = getItem(i);
        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.list_row, g, false);
        }
        ((TextView) v.findViewById(R.id.row_title)).setText("Title: " + p.getTitle());
        ((TextView) v.findViewById(R.id.row_description)).setText("Description: " + p.getDescription());
        ((TextView) v.findViewById(R.id.row_starting_bid)).setText("Bid: $" + String.valueOf(p.getStartingBid()));
        ((TextView) v.findViewById(R.id.row_buy_now)).setText(String.valueOf("Buy Now: $" + p.getBuyNowPrice()));
        final String imageUrlStr = p.getPrimaryImage();
        Picasso.with(getContext()).load(imageUrlStr).into((ImageView) v.findViewById(R.id.row_image));

        v.setBackgroundColor(i % 2 == 0 ? BgColor1 : BgColor2);

        return v;
    }
}
