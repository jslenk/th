package com.tophatter.example.tophatter;

import java.util.*;

import retrofit2.*;
import retrofit2.http.*;

public interface EndpointApi {

    public static final String BASE_URL_STR = "https://tophatter.com/merchant_api/v1/";

    @GET("products.json")
    Call<List<JsonModel.Product>> getProducts();

    @POST("products/update.json")
    Call<JsonModel.Product> updateProduct(@Body JsonModel.Product updated);
}
