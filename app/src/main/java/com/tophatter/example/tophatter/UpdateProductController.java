package com.tophatter.example.tophatter;

import android.util.*;

import com.google.common.eventbus.*;

import java.util.*;

import retrofit2.*;

public class UpdateProductController implements Callback<JsonModel.Product> {

    public interface UpdateError {
        public Response getResponse();
    }

    private final EventBus eventBus;
    private final JsonModel.Product updated;

    public UpdateProductController(final EventBus eventBus, final JsonModel.Product updated) {
        this.eventBus = eventBus;
        this.updated = updated;
    }

    public void start(final EndpointApi api) {
        Call<JsonModel.Product> call = api.updateProduct(updated);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<JsonModel.Product> call, final Response<JsonModel.Product> response) {
        if (response.isSuccessful()) {
            JsonModel.Product p = response.body();
            eventBus.post(p);
        } else {
            Log.e("TAG", response.errorBody().toString());
            eventBus.post(new UpdateError() {
                @Override
                public Response getResponse() {
                    return response;
                }
            });
        }
    }

    @Override
    public void onFailure(Call<JsonModel.Product> call, Throwable t) {
        Log.e("TAG", Arrays.toString(t.getStackTrace()));
    }
}