package com.tophatter.example.tophatter;

import android.app.*;
import android.app.AlertDialog;
import android.content.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;

import com.google.common.base.*;
import com.google.common.collect.*;
import com.google.common.eventbus.*;
import com.google.gson.*;

import java.io.*;
import java.util.*;

import okhttp3.*;
import okhttp3.logging.*;
import retrofit2.*;
import retrofit2.converter.gson.*;

import static com.tophatter.example.tophatter.ProductDetailsActivity.*;

public class MainActivity
        extends AppCompatActivity
        implements AdapterView.OnItemClickListener {

    private static final String API_TOKEN = "6db232c5843372d50d6dddbf5ea33570";
    public final static EventBus eventBus = new EventBus(); // todo: via DI framework.
    public static EndpointApi api; // todo: via DI framework.
    private ListView listView;
    private List<JsonModel.Product> products;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listview);
        listView.setOnItemClickListener(this);
        eventBus.register(this);
        final Gson gson = new GsonBuilder().setLenient().create();
        final okhttp3.OkHttpClient httpClient = new okhttp3.OkHttpClient()
                .newBuilder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        okhttp3.Request original = chain.request();
                        HttpUrl originalHttpUrl = original.url();
                        HttpUrl url = originalHttpUrl.newBuilder()
                                .addQueryParameter("access_token", API_TOKEN)
                                .build();
                        okhttp3.Request.Builder requestBuilder = original.newBuilder()
                                .url(url);
                        okhttp3.Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                })
//               // todo: disable unless debugging.
//                .addInterceptor(
//                        new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
//                )
                .build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EndpointApi.BASE_URL_STR)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build();
        api = retrofit.create(EndpointApi.class);
        new GetProductsController().start(api);
    }

    @Subscribe
    public void onProducts(final List<JsonModel.Product> l) {
        if (l != null) {
            updateListAdapter(l);
            findViewById(R.id.wait_spinner).setVisibility(View.GONE);
            findViewById(R.id.list_layout).setVisibility(View.VISIBLE);
        }
    }

    private void updateListAdapter(final List<JsonModel.Product> l) {
        products = l;
        final ProductListAdapter a = new ProductListAdapter(this, products);
        listView.setAdapter(a);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == listView) {
            final JsonModel.Product p = (JsonModel.Product) parent.getAdapter().getItem(position);
            final Intent i = new Intent(this, ProductDetailsActivity.class);
            i.putExtra(EXTRA_PRODUCT_ORIGINAL_KEY, p);
            startActivityForResult(i, 0);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            switch (resultCode) {
                case Activity.RESULT_OK: {
                    final JsonModel.Product updated = data == null ? null : (JsonModel.Product) data.getSerializableExtra(EXTRA_PRODUCT_UPDATED_KEY);
                    if (updated != null) {
                        updateProductList(updated);
                        updateListAdapter(products);
                    }
                }
                case Activity.RESULT_CANCELED: {
                    final String msg = data == null ? null : (data.hasExtra(EXTRA_MESSAGE_KEY) ? data.getStringExtra(EXTRA_MESSAGE_KEY) : null);
                    if (msg != null) {
                        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage(msg);
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Dangit!",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                }
            }
        }
    }

    private void updateProductList(final JsonModel.Product updated) {
        products = Lists.transform(products, new Function<JsonModel.Product, JsonModel.Product>() {
            @Nullable
            @Override
            public JsonModel.Product apply(JsonModel.Product input) {
                return input.getIdentifier().equals(updated.getIdentifier()) ? updated : input;
            }
        });
    }

    @Subscribe
    public void onProduct(final JsonModel.Product updated) {
        if (updated != null) {
            updateProductList(updated);
        }
    }
}
